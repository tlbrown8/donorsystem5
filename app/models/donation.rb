class Donation < ActiveRecord::Base
  belongs_to :donor

  def self.amount
    donation.sum
  end
end

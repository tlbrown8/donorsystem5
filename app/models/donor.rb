class Donor < ActiveRecord::Base
  has_many :donations

  def amount2
      self.donations.sum(:amount)
  end

end

class CreateDonors < ActiveRecord::Migration
  def change
    create_table :donors do |t|
      t.string :name
      t.string :email_address
      t.decimal :amount
      t.timestamp :donation_date

      t.timestamps
    end
  end
end
